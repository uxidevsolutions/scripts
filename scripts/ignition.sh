#!/bin/bash

# rename the node hostnamectl set-hostname master
# by killabout69@gmail.com https://regner.com.mx
#
# _    ___                  _        _             _ _   _             
#| | _( _ )       ___  __ _| |_     (_) __ _ _ __ (_) |_(_) ___  _ __  
#| |/ / _ \ _____/ __|/ _` | __|____| |/ _` | '_ \| | __| |/ _ \| '_ \ 
#|   < (_) |_____\__ \ (_| | ||_____| | (_| | | | | | |_| | (_) | | | |
#|_|\_\___/      |___/\__, |\__|    |_|\__, |_| |_|_|\__|_|\___/|_| |_|
#                     |___/            |___/    
#     

sudo hostnamectl set-hostname master
sudo setenforce 0
sudo sed -i 's/^SELINUX=enforcing$/SELINUX=permissive/' /etc/selinux/config

sudo sed -i '/swap/d' /etc/fstab
sudo swapoff -a

yes | sudo yum install -y yum-utils
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum-config-manager --enable docker-ce-nightly
sudo yum install docker-ce docker-ce-cli containerd.io -y 

sudo systemctl enable docker
sudo systemctl start docker






cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

sudo yum install -y kubelet kubeadm kubectl --disableexcludes=kubernetes
sudo systemctl enable kubelet
sudo systemctl start kubelet

sudo yes | kubeadm init --pod-network-cidr=10.244.0.0/16
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
export ARCH=amd64
curl -sSL "https://github.com/coreos/flannel/blob/master/Documentation/kube-flannel.yml?raw=true"  | sed "s/amd64/${ARCH}/g" | kubectl create -f -
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml 
cp ~/.kube/config /home/sgtmaster/
